import React from "react";

interface IButtonLayoutInterface {
  title: string;
  type?: "button" | "submit" | "reset";
  onClick?: () => void;
}

const ButtonLayout = ({ title, onClick, type }: IButtonLayoutInterface) => {
  return (
    <button className="button" type={type || "button"} onClick={onClick}>
      {title}
    </button>
  );
};
export default ButtonLayout;
