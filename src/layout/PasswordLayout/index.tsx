import React, { useState } from "react";

interface IPasswordLayoutInterfact {
  id: string;
  type?: string;
  value?: string;
  placeholder?: string;
  onChange: (e: any) => void;
}

const PasswordLayout = ({
  id,
  type,
  value,
  placeholder,
  onChange
}: IPasswordLayoutInterfact) => {
  // const [inputValue, setInputValue] = useState("");

  // const onChangeInput = (e: any) => {
  //   e.stopPropagation();
  //   setInputValue(e.target.value);
  // };

  const [isHiding, setHiding] = useState(true);
  return (
    <div className="password-container">
      <input
        className="inputPassword"
        autoComplete="false"
        type={isHiding ? "password" : "text"}
        id={id}
        defaultValue={value}
        placeholder={placeholder}
        // value={value}
        onChange={(e) => onChange(e)}
      />
      <img
        className="hide-show-password"
        src={isHiding ? process.env.PUBLIC_URL + "/hide.png" : process.env.PUBLIC_URL + "/show.png"}
        alt="hide-show"
        onClick={() => setHiding((hiding) => !hiding)}
      />
    </div>
  );
};

export default PasswordLayout;
