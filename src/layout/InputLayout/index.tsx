import React, { useState } from "react";

interface IInputLayoutInterfact {
  id: string;
  type: string;
  value?: string;
  placeholder?: string;
  onChange: (e: any) => void;
}

const InputLayout = ({
  id,
  type,
  value,
  placeholder,
  onChange,
}: IInputLayoutInterfact) => {
  // const [inputValue, setInputValue] = useState("");

  // const onChangeInput = (e: any) => {
  //   e.stopPropagation();
  //   setInputValue(e.target.value);
  // };
  return (
    <input
      className="input"
      autoComplete="false"
      type={type}
      id={id}
      defaultValue={value}
      placeholder={placeholder}
      // value={value}
      onChange={(e) => onChange(e)}
    />
  );
};

export default InputLayout;
