export const KEY = {
  URL_SAMPLE: 'SAMPLE',
  CAPTCHA_SITE_KEY: '6LcTWQIeAAAAALIQ3gkGL3b0ImDS8wQflo4f5n8P',
  USERNAME_COOKIE_KEY: 'token',
  VISITOR_TOKEN: 'visitor_token',
  USER_INFO: 'user',
  SESSION_NAME_CAPTCHA: 'captcha',
  REFRESH_TOKEN: 'refresh_token',
  PUBLIC_KEY: 'public_key',
  IS_LOGOUT: 'is_logout',
  INVALID_TOKEN: 'invalid_token',
}

export const ErrorVerifyMapping: {
  [key: string]: {
    raw: string;
    mgs: {
        vn: string;
        eng: string;
    };
};
} = {
  "01": {
    raw: "Unauthorized",
    mgs: {
      vn: "Quyền truy cập lỗi.",
      eng: "Unauthorized."
    }
  },
  "02": {
    raw: "Key expired or not match",
    mgs: {
      vn: "Key không khớp hoặc hết hạn.",
      eng: "Key expired or not match."
    }
  },
  "03": {
    raw: "User not exist",
    mgs: {
      vn: "Người dùng không tồn tại.",
      eng: "User not exist."
    }
  },
  "04": {
    raw: "SessionId not found",
    mgs: {
      vn: "SessionId không tìm thấy.",
      eng: "SessionId not found."
    }
  },
  "05": {
    raw: "Format message invalid",
    mgs: {
      vn: "Lỗi không đúng định dạng.",
      eng: "Format message invalid."
    }
  },
  "06": {
    raw: "Password expired",
    mgs: {
      vn: "Mật khẩu hết hạn.",
      eng: "Password expired."
    }
  },
  "07": {
    raw: "Key expired or data invalid",
    mgs: {
      vn: "Key hết hạn hoặc dữ liệu không đúng.",
      eng: "Key expired or data invalid."
    }
  },
  "08": {
    raw: "BankAccount not found",
    mgs: {
      vn: "Tài khoản ngân hàng không tìm thấy.",
      eng: "BankAccount not found"
    }
  },
  "09": {
    raw: "BankAccount not active",
    mgs: {
      vn: "Tài khoản ngân chưa được bật để  hoạt động.",
      eng: "BankAccount not active"
    }
  },
  "10": {
    raw: "BankAccount's ccy invalid",
    mgs: {
      vn: "CCY tài khoản ngân không đúng.",
      eng: "BankAccount's ccy invalid"
    }
  },
  "11": {
    raw: "Create token fail",
    mgs: {
      vn: "Tạo token lỗi.",
      eng: "Create token fail"
    }
  },
  "12": {
    raw: "User is disabled",
    mgs: {
      vn: "Người dùng không được kích hoạt.",
      eng: "User is disabled"
    }
  },
  "13": {
    raw: "Password was used before",
    mgs: {
      vn: "Mật khẩu đã được sử dụng trước đó.",
      eng: "Password was used before"
    }
  },
  "14": {
    raw: "Password invalid",
    mgs: {
      vn: "Mật khẩu không đúng.",
      eng: "Password invalid"
    }
  },
  "15": {
    raw: "User already exists",
    mgs: {
      vn: "Người dùng đã tồn tại.",
      eng: "User already exists"
    }
  },
  "16": {
    raw: "Forbidden",
    mgs: {
      vn: "Forbidden lỗi hệ thống.",
      eng: "Forbidden"
    }
  },
  "17": {
    raw: "Forbidden",
    mgs: {
      vn: "Key không được tìm thấy hoặc hết hạn.",
      eng: "Key not found or expired"
    }
  },
  "18": {
    raw: "Account not allow to tokenize",
    mgs: {
      vn: "Tài khoản không thể tạo token.",
      eng: "Account not allow to tokenize"
    }
  },
  "19": {
    raw: "User is not active",
    mgs: {
      vn: "Người dùng không hoạt động.",
      eng: "User is not active"
    }
  },
  "20": {
    raw: "Account is already tokenized",
    mgs: {
      vn: "Người dùng không hoạt động.",
      eng: "Tài khoản đã được mã hóa"
    }
  },
  "21": {
    raw: "Account or user banking not existed",
    mgs: {
      vn: "Tài khoản hoặc người dùng không tồn tại.",
      eng: "Account or user banking not existed"
    }
  },
  "22": {
    raw: "Password used",
    mgs: {
      vn: "Mật khẩu đã được sử dụng.",
      eng: "Password used"
    }
  },
  "99": {
    raw: "System error",
    mgs: {
      vn: "Lỗi hệ thống.",
      eng: "System error"
    }
  },
  "00": {
    raw: "Success",
    mgs: {
      vn: "Thành công.",
      eng: "Success"
    }
  }
}