import NodeRSA from "node-rsa";
import { KEY } from "../common";
interface IPublicKey {
  enabled: boolean;
  publicKey: string;
}
export const encryptDataRSA = async (data: any): Promise<any | undefined> => {
  const publicKey = window.localStorage.getItem(KEY.PUBLIC_KEY);
  if (!publicKey) {
    return data;
  }

  // console.log(data)
  // const publicKeyObject: IPublicKey = JSON.parse(publicKey)

  // if (!publicKeyObject.enabled) {
  //   return data
  // }

  // Load key from PEM string

  // const key = new NodeRSA({ b: 2048 });
  const keyData =
    "-----BEGIN PUBLIC KEY-----\n" + publicKey + "-----END PUBLIC KEY-----";

  const key = new NodeRSA(keyData, "public", {
    encryptionScheme: "pkcs1",
    signingScheme: "pkcs1",
  });
  const encryptedString = key.encrypt(data, "base64");
  // const decrypted = key.decrypt(encryptedString, "utf8");
  // console.log("decrypted: ", decrypted);
  // console.log(encryptedString);
  return { body: encryptedString };
};
