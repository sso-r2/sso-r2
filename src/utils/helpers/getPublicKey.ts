import { DOMAIN } from "../domain";

const getPublicKey = async () => {
  const apiUrl = `${DOMAIN.API_OATH_DOMAIN}/get_key`;
  const resp = await fetch(apiUrl);
  // console.log(resp);
  return resp.json();
};

export default getPublicKey;
