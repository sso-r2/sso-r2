import { KEY } from "../common";
import { DOMAIN } from "../domain";
import { encryptDataRSA } from "./encryptDataRSA";
import { v4 as uuidv4 } from "uuid";

const verifyAccount = async (data: any) => {
  const publicKey = window.localStorage.getItem(KEY.PUBLIC_KEY);
  const apiUrl = `${DOMAIN.API_OATH_DOMAIN}/verify`;

  const rsaData = await encryptDataRSA(JSON.stringify(data));
  console.log("rsa", rsaData.body);
  console.log("public", publicKey);
  console.log(apiUrl);
  const databody = {
    request: {
      requestId: uuidv4(),
    },
    data: {
      credential: rsaData.body,
      key: publicKey,
    },
  };

  try {
    const resp = await fetch(apiUrl, {
      method: "POST", // or 'PUT'
      // mode: "no-cors",
      body: JSON.stringify(databody),
      headers: {
        "Content-Type": "application/json; charset= utf8",

        //     // "Content-Encoding": "gzip, deflate, br",
        //     // "X-IBM-CLIENT-SECRET": "98WVLs9FsS36Ep22BlyCRttlSZqbaik4RfaFaYxj",
      },
    });

    // const resp = await fetch(apiUrl, {
    //   method: "POST",
    //   headers: {
    //     'Content-Type': 'application/json; charset= utf8',
    //     // "Content-Encoding": "gzip, deflate, br",
    //     // "X-IBM-CLIENT-SECRET": "98WVLs9FsS36Ep22BlyCRttlSZqbaik4RfaFaYxj",
    //   },
    //   mode: "no-cors",
    //   body: JSON.stringify(databody),
    // });
    return resp.json();
  } catch (e) {
    return e;
  }

  // var myHeaders = new Headers();
  // myHeaders.append("Content-Type", "application/json");

  // var raw = JSON.stringify({
  //   request: {
  //     requestId: "{{messageid}}",
  //     requestTime: "2023-02-06 13:23:29",
  //   },
  //   data: {
  //     credential:
  //       "CNqRUghHb84tRlmDaBhqzTd6p3qsoDL/BWY5COFS34wV4zueY78Ebt+43w0AlU46lzwd9PctGOtgDpyatadibwWP8bsG8MZUUyXJdQOROI/9fYvsyuKoW4vptNCyN/KF4y26niLiaqhNY0xwxO0FJL9vnZsRqXsCv/sD6QzhELQ=",
  //     key: "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCDY1DzbqoavP8UVPYARHpy+zPlaFiBdf3imr5m4RdbHCwMueevk+NoWV2dqL/LBnk8oWMqWkgMDnTleXe/jvj6zQEuuCoBVDiZq4k0JXbHdTmXg0/fH7d9YD0BsSkpSJH8A9RBSnjvIzKLNHXKTUyxG1QIIKbU2lhVAB/jK2UtdwIDAQAB",
  //   },
  // });

  // var requestOptions = {
  //   method: "POST",
  //   headers: myHeaders,
  //   body: raw,
  //   redirect: "follow",
  // };

  // fetch("https://sso-uat.hdbank.com.vn/oauth2/api/verify", {
  //   method: "POST",
  //   headers: myHeaders,
  //   mode: "no-cors",
  //   body: raw,
  //   redirect: "follow",
  // })
  //   .then((response) => response.text())
  //   .then((result) => console.log(result))
  //   .catch((error) => console.log("error", error));
};

export default verifyAccount;
