import React, { lazy } from "react";
import "./App.css";
import HomePageComponent from "./component/HomePageComponent";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import LoadingComponent from "./component/LoadingComponent";
import { QueryClient, QueryClientProvider } from "react-query";
// const HomePageComponent = lazy(() => import("./component/HomePageComponent"));

function App() {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });

  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <div className="container">
          <BrowserRouter basename="/r2sso">
            <Switch>
              <Route path="/admin/:code?">
                <LoadingComponent />
              </Route>
              <Route exact path="/">
                <HomePageComponent />
              </Route>
              {/* <Route path="/index/:id?" element={<HomePageComponent />} /> */}
            </Switch>
          </BrowserRouter>
        </div>
      </div>
    </QueryClientProvider>
  );
}

export default App;
