import React, { useState } from "react";
import ButtonLayout from "../layout/ButtonLayout";
import InputLayout from "../layout/InputLayout";
import PasswordLayout from "../layout/PasswordLayout";
import { encryptDataRSA } from "../utils/helpers/encryptDataRSA";
import { useHistory, useLocation } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import verifyAccount from "../utils/helpers/verifyAccount";
import { useQuery } from "react-query";
import { ErrorVerifyMapping, KEY } from "../utils/common";
import { DOMAIN } from "../utils/domain";
import LoadingComponent from "./LoadingComponent";

const LoginComponent = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  // const text = "Hello RSA!";
  const urlParams = new URLSearchParams(useLocation().search);
  
  React.useEffect(()=>{
      setUsername(urlParams.get("username")  || '')
  },[])
  // const { privateKey, publicKey } = encryptRsa.createPrivateAndPublicKeys();
  const history = useHistory();

  const onChangeInput = (e: any) => {
    e.stopPropagation();
    setUsername(e.target.value);
  };

  const onChangePassword = (e: any) => {
    e.stopPropagation();
    setPassword(e.target.value);
  };

  const onClick = async () => {
    // const accountString = JSON.stringify({
    //   username,
    //   password,
    // });
    setLoading(true);
    // const data = await encryptDataRSA(
    //   JSON.stringify({
    //     username,
    //     password,
    //   })
    // );
    // console.log(data);

    const verify = await verifyAccount({
      username,
      password,
    });
    // const { data } = useQuery("publicKeyState", getPublicKey);
    // navigate(`/index?code=${codeID}`);
    // window.location.replace(`/index?code=${codeID}`)
    console.log("verify: ", verify);

    if (verify?.response?.responseCode === "00") {
      if (verify?.data?.code !== null) {
        setLoading(false);
        window.parent.postMessage(
          {
            type: "login_success",
            code: `${verify?.data?.code}`,
            message: `${verify?.response?.responseMessage}`,
          },
          "*"
        );
      }
      if (verify?.response?.responseCode === "01") {
        setLoading(false);
        window.parent.postMessage(
          {
            type: "login_unauthorized",
            code: `${verify?.response?.responseCode}`,
            message: `${verify?.response?.responseMessage}`,
          },
          "*"
        );
        // history.push({ pathname: "/admin", search: `?code=${uuidv4()}` });
      }
    }else {
      setLoading(false);
        window.parent.postMessage(
          {
            type: "login_failed",
            code: `${verify?.response?.responseCode}`,
            message: ErrorVerifyMapping[verify?.response?.responseCode]?.mgs?.vn || "Lỗi hệ thống vui lòng thử  lại!" , //`${verify?.response?.responseMessage}`,
          },
          "*"
        );
    }
    return;
  };

  // useEffect(() => {
  //   navigate({
  //     pathname: "/index",
  //     search: createSearchParams({
  //       id: codeID,
  //     }).toString(),
  //   });
  // }, [data, codeID, navigate]);
  return (
    <>
      {loading && <LoadingComponent />}
      <InputLayout
        type="text"
        id="username"
        value={username}
        onChange={onChangeInput}
        placeholder="Tên đăng nhập HDBank eBanking"
      />
      <PasswordLayout
        id="password"
        placeholder="Mật khẩu HDBank eBanking"
        value={password}
        onChange={onChangePassword}
      />
      {/* <div className="forgotPassword">
        <span style={{ marginRight: "28px" }}>Quên mật khẩu ?</span>
      </div> */}
      <ButtonLayout title="Đăng nhập" onClick={onClick} />
    </>
  );
};

export default LoginComponent;
