import React from "react";

const LoadingComponent = () => {
  return (
    <div className="loaderContainer">
      <div className="loader"></div>
    </div>
  );
};

export default LoadingComponent;
