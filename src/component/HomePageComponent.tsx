import React, { useEffect, useState, useCallback } from "react";
import getPublicKey from "../utils/helpers/getPublicKey";
import LoginComponent from "./LoginComponent";
import RegisterComponent from "./RegisterComponent";
import { useQuery } from "react-query";
import { DOMAIN } from "../utils/domain";
import { KEY } from "../utils/common";

import { useLocation } from "react-router-dom";

const PAGE_TYPE = {
  REGISTER: "register",
  LOGIN: "login",
};

const getType = (urlParams: URLSearchParams)=>{
  const t = urlParams.get("type") 
  console.log(t);
  
  if (t !== PAGE_TYPE.LOGIN && t !== PAGE_TYPE.REGISTER){
    return PAGE_TYPE.LOGIN
  }
  return t
}
const HomePageComponent = () => {
  const urlParams = new URLSearchParams(useLocation().search);
  const [pageType, setPageType] = useState(getType(urlParams));

  // const getPublicKeyCallback = useCallback(async () => {
  //   return await getPublicKey();
  // }, []);

  // useEffect(() => {
  //   const apiUrl = `${DOMAIN.API_OATH_DOMAIN}/get_key`;
  //   // const  =  fetch(apiUrl);
  //   fetch(apiUrl).then((resp) => console.log(resp));
  //   console.log("alo");
  // }, []);

  const { data } = useQuery("publicKeyState", getPublicKey);
  const publicKey = data?.data?.key;

  // console.log(publicKey);

  useEffect(() => {
    if (publicKey) window.localStorage.setItem(KEY.PUBLIC_KEY, publicKey);
  }, [publicKey]);

  const getDataFromIframe = useCallback((msg: MessageEvent<any>) => {
    if (msg.data.type === "register") {
      setPageType(PAGE_TYPE.REGISTER);
    }
    if (msg.data.type === "login") {
      setPageType(PAGE_TYPE.LOGIN);
    }
  }, []);

  useEffect(() => {
    window.addEventListener("message", getDataFromIframe);
    return () => {
      window.removeEventListener("message", getDataFromIframe);
    };
  }, [getDataFromIframe]);
  return (
    <>
      {pageType === PAGE_TYPE.LOGIN && <LoginComponent />}
      {pageType === PAGE_TYPE.REGISTER && <RegisterComponent />}
    </>
  );
};

export default HomePageComponent;
