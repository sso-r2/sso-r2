import React from "react"

const InfoCardComponent = () => {
  return (
    <div className="infoCardContainer">
      <img src={process.env.PUBLIC_URL +"/icon.png"} className="infoCardIcon" alt="info"/>
      <div className="infoDescription">Bằng việc nhấn tiếp tục, user đồng ý rằng HDB được quyền truy xuất thông tin của user tại HDB để hiển thị ở màn hình tiếp theo và tiến hành việc đăng ký.</div>
    </div>
  )
}

export default InfoCardComponent